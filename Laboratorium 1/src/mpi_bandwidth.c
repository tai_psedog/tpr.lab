#include <stdio.h>
#include "mpi.h"

#define MASTER 0
#define SLAVE 1
#define MAX_MESSAGE_SIZE 10000001
#define ITERATION_NUMBER 10
#define PACKETS 10

int main(int argc, char **argv)
{
	int rank, size;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	int i;
	int message_size = 100;
	for (i = 1; i < 6; ++i)  //that loop multiplicate size x10
	{
		if (rank == MASTER)
		{
			master_bandwidth(message_size);
		} 
		else
		{
			slave_bandwidth(message_size);
		}
		
		message_size *= 10;
	}
	
	MPI_Finalize();
	return 0;
}

int master_bandwidth(int message_size)
{
	double start_time, end_time;
	char buf[MAX_MESSAGE_SIZE];
	int it;
	int current_message_size = message_size;
	
	MPI_Status status;
	
	for (it = 1; it < ITERATION_NUMBER; ++it) //that loop makes 1,2,3,4... at the beginning of datasize
	{
		start_time = MPI_Wtime();

		int it2;
		for (it2 = 0; it2 < PACKETS; ++it2) //that loops makes PACKETS attempts to transfer data by current_message_size chunks
		{
			MPI_Send(buf, current_message_size, MPI_CHARACTER, SLAVE, 0, MPI_COMM_WORLD);
		}
		
		end_time = MPI_Wtime();
		
		double time = end_time - start_time;
		double bandwidth = (current_message_size * PACKETS)/(time * 1024.0 * 1024.0);
	    
		printf("%-20d", current_message_size);
		printf("%-20f\n",bandwidth);
		
		current_message_size += message_size;
	}
}

int slave_bandwidth(int message_size)
{
	double start_time, end_time;
	char buf[MAX_MESSAGE_SIZE];
	int it;
	int current_message_size = message_size;
	
	MPI_Status status;
	
	for (it = 1; it < ITERATION_NUMBER; ++it)
	{
		int it2;
		for (it2 = 0; it2 < PACKETS; ++it2)
		{
			MPI_Recv(buf, current_message_size, MPI_CHARACTER, MASTER, 0, MPI_COMM_WORLD, &status);
		}
		
		current_message_size += message_size;
	}
}
