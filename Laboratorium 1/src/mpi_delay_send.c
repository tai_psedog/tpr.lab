#include <stdio.h>
#include "mpi.h"

#define MASTER 0
#define SLAVE 1
#define ITERATION_NUMBER 10000
#define MAX_MESSAGE_SIZE 10000001

int main(int argc, char **argv)
{
	int rank, size;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int i;
	int message_size = 100;
	for (i = 1; i < 6; ++i)  //that loop multiplicate size x10
	{
	
		if (rank == MASTER)
		{
			master_delay(message_size);
		} 
		else
		{
			slave_delay(message_size);
		}
		
		message_size *= 10;
	}
	
	MPI_Finalize();
	return 0;
}

int master_delay(int message_size)
{
	double start_time, end_time;
	char buf;
	int it, it2;
	int current_message_size = message_size;
	double divider = (double) 2.0 * ITERATION_NUMBER;
	MPI_Status status;
	
	
	for (it2 = 1; it2 < ITERATION_NUMBER; ++it2) //that loop makes 1,2,3,4... at the beginning of datasize
	{
	
		start_time = MPI_Wtime();
		
		for (it = 1; it < ITERATION_NUMBER; ++it)
		{
			MPI_Send(&buf, current_message_size, MPI_CHARACTER, SLAVE, 0, MPI_COMM_WORLD);
			MPI_Recv(&buf, current_message_size, MPI_CHARACTER, SLAVE, 0, MPI_COMM_WORLD, &status);
		}
		
		end_time = MPI_Wtime();
		
		double time = (end_time - start_time)/divider;
		printf("%-20d", current_message_size);
		printf("%-20f\n",time);
			
		current_message_size += message_size;
	}
}

int slave_delay(int message_size)
{
	char buf[MAX_MESSAGE_SIZE];
	int it,it2;
	int current_message_size = message_size;
	
	MPI_Status status;
		
	for (it2 = 1; it2 < ITERATION_NUMBER; ++it2) //that loop makes 1,2,3,4... at the beginning of datasize
	{
		
		for (it = 1; it < ITERATION_NUMBER; ++it)
		{
			MPI_Recv(&buf, current_message_size, MPI_CHARACTER, MASTER, 0, MPI_COMM_WORLD, &status);
			MPI_Send(&buf, current_message_size, MPI_CHARACTER, MASTER, 0, MPI_COMM_WORLD);
		}
	
		current_message_size += message_size;
	}
}
