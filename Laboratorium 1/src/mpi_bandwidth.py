#!/usr/bin/env python

from mpi4py import MPI
import numpy

#-----Basic constants

DEFAULT_PACKETS_PER_MEASUREMENT = 10
DEFAULT_START_MESSAGE_SIZE = 100
DEFAULT_ITERS_COUNT = 10

#-----Communication type handlers


def get_standard_communication(communicator, is_send):
    if is_send:
        return communicator.Send
    else:
        return communicator.Recv


def get_prep_communication(communicator, is_send):
    if is_send:
        return communicator.Ssend
    else:
        return communicator.Recv


mpi_communication_functions = {
    'std': (get_standard_communication, "standard blocking"),
    'prep': (get_prep_communication, "synchronous blocking")
}

#-----Work handlers


def handle_master_work(communicator, comm_type):
    communication_tuple = mpi_communication_functions[comm_type]
    send_function = communication_tuple[0](communicator, True)
    print '=' * 80 + '\nThis program measures MPI bandwidth for %s communication\n' % communication_tuple[1] + '=' * 80
    print 'Message size (MB)'.ljust(20) + ' ' + 'Bandwidth (MB/S)'.ljust(20)

    message_size = DEFAULT_START_MESSAGE_SIZE
    for i in range(6):
        current_message_size = message_size

        for iteration in range(DEFAULT_ITERS_COUNT):
            message = numpy.arange(current_message_size / 4, dtype=numpy.int32)

            start_time = MPI.Wtime()
            for packet_iteration in range(DEFAULT_PACKETS_PER_MEASUREMENT):
                send_function(message, dest=1)
            finish_time = MPI.Wtime()

            time_per_message = (finish_time - start_time) / float(DEFAULT_PACKETS_PER_MEASUREMENT)
            real_packet_mb_size = message.nbytes / float(1024 ** 2)
            bandwidth = real_packet_mb_size / time_per_message

            print str(real_packet_mb_size).ljust(20) + ' ' + str(bandwidth).ljust(20)

            current_message_size += message_size

        message_size *= 10


def handle_slave_work(communicator, comm_type):
    communication_tuple = mpi_communication_functions[comm_type]
    recv_function = communication_tuple[0](communicator, False)

    message_size = DEFAULT_START_MESSAGE_SIZE

    for i in range(6):
        current_message_size = message_size

        for iteration in range(DEFAULT_ITERS_COUNT):
            message = numpy.empty(current_message_size / 4, dtype=numpy.int32)

            for packet_iteration in range(DEFAULT_PACKETS_PER_MEASUREMENT):
                recv_function(message, source=0)

            current_message_size += message_size

        message_size *= 10


work_handler = {
    0: handle_master_work,
    1: handle_slave_work
}


def handle_err_rank(communicator, dummy_variable):
    pass


#-------Arguments section

import sys

argv = sys.argv[1:]
if len(argv) != 1 or ( argv[0] != 'std' and argv[0] != 'prep'):
    print 'usage: ./mpi_bandwidth.py { std | prep }\n\twhere:\n\t\tstd - is for standard communication\
          \n\t\tprep - is for blocking synchronous communication'
    sys.exit(1)

#-------MPI work start section

comm = MPI.COMM_WORLD
my_rank = comm.Get_rank()

worker = work_handler.get(my_rank, handle_err_rank)
worker(comm, argv[0])

MPI.Finalize()
