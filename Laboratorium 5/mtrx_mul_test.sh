#!/bin/bash

small_probl_dim_1=10
small_probl_dim_common=50

big_probl_dim_1=15
big_probl_dim_common=10000

proc_count=(3 4 5 6 7 8 9 10 11 12 13 14 15 16)


function do_work() # [$1:$2] x [$2:$1] muliplication per $3 processes log to $4
{
	mpiexec -machinefile ./mpihosts -np $3 ./mtrx_mul $1 $2 $2 $1 0 >> $4
}


function pr_test() # fixed size muliplication tests of matrixes [$1:$2] [$2:$1] of scalable/not scalable problem ($3) log to $4
{
        if [[ $3 -eq 0 ]]; then
		echo "--------=FIXED SIZE PROBLEM [$1:$2] x [$2:$1] =------------ " >> $4
	else
		echo "--------= SCALABLE PROBLEM of basize size  [$1:$2] x [$2:$1] =------------ " >> $4
	fi

	echo "-- time for 1 computing process: " >> $4

	do_work $1 $2 2 $4

	echo "-- other times: " >> $4

	for i in ${proc_count[@]}; do
		size=$2
		if [[ $3 -eq 1 ]]; then
			size=$(( ($i - 1) * $2 ))
		fi
		do_work $1 $size $i $4
	done

	echo "" >> $4
}

function test()
{
	log_file="r_log_`date +%H:%M:%S_%N`"
	echo "--testing fixed-size problems"
	pr_test $small_probl_dim_1 $small_probl_dim_common 0 $log_file
	pr_test $big_probl_dim_1 $big_probl_dim_common 0 $log_file
	echo "--testing scalable problems"
	pr_test $small_probl_dim_1 $small_probl_dim_common 1 $log_file
	pr_test $big_probl_dim_1 $big_probl_dim_common 1 $log_file
}

for c in {1..6}; do
	echo "---------------START $c test--------------"
	test;
done
