#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>   

#define ROOT_ID 0

double ** allocate_matrix(int height, int width, int continiouns)
{
    double ** result = (double**) malloc(height * sizeof(double*));
    int idx = 0;
    
    if(continiouns)
    {
        double * send_buff = (double*) malloc(height * width * sizeof(double));
        for (idx = 0; idx < height; ++idx) {
            *(result + idx) = send_buff + idx * width;
        }    
    }else{
        for (idx = 0; idx < height; ++idx) {
            result[idx] = (double*) malloc(width*sizeof(double));
        }
    }
    
    return result;
}

void free_continious_matrix(double ** mtrx)
{
    if(mtrx)
    {
        free(*mtrx);
        free(mtrx);
    }       
}

void free_uncontinious_matrix(double ** mtrx, int height)
{
    if(mtrx)
    {
        int idx;
        for(idx=0; idx < height; ++idx) free(mtrx[idx]);
        free(mtrx);
    }
}

void fill_matrix(double ** matrix, int height, int width)
{
    int c = 1;
    int i, j;
    for(i = 0; i < height; ++i) for(j = 0; j < width; ++j) matrix[i][j] = c++;
}

void print_vector(double * vector, int size)
{
    int j;
    for (j = 0; j < size; ++j)
        printf("%6.1lf", vector[j]);
    putchar('\n');
}

void print_matrix(const char * matrix_name, double **matrix, int h, int w)
{
    printf("Matrix \'%s\'\n", matrix_name);
    int i;
    for (i = 0; i < h; ++i) {
        print_vector(matrix[i], w);
    }
    putchar('\n');
}

double * calc_m_row(double ** table, int number) { return table[number]; }
double * calc_m_column(double ** table, int number){ return *table + number; }

int  parse_arguments(int argc, char ** argv, int * mtrx1_height, int * mtrx1_width, int * mtrx2_height, int * mtrx2_width)
{
    if (argc < 6) {
        fprintf(stderr, "usage: %s first_matrix_height first_matrix_width second_matrix_height second_matrix_width verbose_flag\n", *argv);
        exit(EXIT_FAILURE);
    }

    *mtrx1_height = atoi(argv[1]);
    *mtrx1_width = atoi(argv[2]);
    *mtrx2_height = atoi(argv[3]);
    *mtrx2_width = atoi(argv[4]);

    if(*mtrx1_width != *mtrx2_height)
    {
        fprintf(stderr, "matrix dimensiond do not match: m1 [%dx%d] m2 [%dx%d]", *mtrx1_height, * mtrx1_width, *mtrx2_height, *mtrx2_width );
        exit(EXIT_FAILURE);
    }
    
    return atoi(argv[5]);
}

int main(int argc, char **argv)
{
    int m1_height, m1_width, m2_height, m2_width;
    
    int verbose_fl = parse_arguments(argc, argv, &m1_height, &m1_width, &m2_height, &m2_width);
    
    int res_mtrx_height = m1_height, res_mtrx_width = m2_width;
    long cells_count = res_mtrx_height * res_mtrx_width;
    
    int world_rank;
    int world_size;
    
    double start_time, end_time;
    
    // MPI rutines
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    if (world_size < 2) {
        fprintf(stderr, "World size must be greater than 1 for %s\n", argv[0]);
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    
    const int actual_working_processes = world_size - 1 < cells_count ? world_size - 1 : cells_count;

    if(world_rank <= actual_working_processes)
    {
        // calculate cells count for each process
        
        long cells_per_process[actual_working_processes + 1];
        long cells_per_process_avg = cells_count / actual_working_processes;
        long cells_count_assigned = 0;
        
        int process_rank;
        // --- set avg cells count for each process
        for(process_rank = 1; process_rank <= actual_working_processes ; ++process_rank)
        {
            cells_per_process[process_rank] = cells_per_process_avg;
            cells_count_assigned += cells_per_process_avg;
        }
        // --- divide reminded cells count between first cells_not_assigned_count processes
        long cells_not_assigned_count = cells_count - cells_count_assigned;
        for(process_rank = 1; cells_not_assigned_count > 0 ; --cells_not_assigned_count, ++process_rank)
        {
            cells_per_process[process_rank] += 1;
        }
        
        // calculate pack send_buffer max size 
        const int send_buff_size = cells_per_process[1] * 2 * m1_width * sizeof(double); 
        const int result_send_buff_size = cells_per_process[1] * sizeof(double);
        
        int i;
        
        // Define new MPI datatypes
        MPI_Datatype m1_row_type;  
        MPI_Datatype m2_column_type;
        
        MPI_Type_vector(1, m1_width, 0, MPI_DOUBLE, &m1_row_type); 
        MPI_Type_commit(&m1_row_type);
        
        MPI_Type_vector(m2_height, 1, m2_width, MPI_DOUBLE, &m2_column_type);  
        MPI_Type_commit(&m2_column_type);
   
        void * send_buff  =  malloc(send_buff_size);
        void * result_send_buff = malloc(result_send_buff_size);
        
       
        
        if (world_rank != ROOT_ID) { //------------- SLAVE
            
            MPI_Barrier(MPI_COMM_WORLD);
            
            long curr_cells_count = cells_per_process[world_rank];

            double results_arr[curr_cells_count];
            
            // declare received columns and rows
            double * rows_received[m1_height];
            double * columns_received[m2_width];
            for(i = 0; i < m1_height; ++i) rows_received[i] = NULL;
            for(i = 0; i < m2_width; ++i) columns_received[i] = NULL;
                  
            int pos;
            int  cell_idx = world_rank - 1;
            int increment = actual_working_processes;
            
            for(i = 0; i < curr_cells_count; ++i)
            {
                pos = 0;
                
                int row = cell_idx / res_mtrx_width;
                int column = cell_idx % res_mtrx_width;
                
                int extr_row = !rows_received[row];
                int extr_column = !columns_received[column];
                
                if(extr_row || extr_column) MPI_Recv(send_buff, send_buff_size, MPI_PACKED, ROOT_ID, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                
                //extract row (if does not have)
                if(extr_row)
                {
                    double * tmp_row = (double*) malloc(m1_width*sizeof(double));
                    rows_received[row] = tmp_row;
                    
                    MPI_Unpack(send_buff, send_buff_size, &pos, tmp_row, m1_width, MPI_DOUBLE, MPI_COMM_WORLD);
                }
                
                //extract column (if does not have)
                if(extr_column)
                {
                    double * tmp_column = (double*) malloc(m2_height * sizeof(double));
                    columns_received[column] = tmp_column;

                    MPI_Unpack(send_buff, send_buff_size, &pos, tmp_column, m2_height, MPI_DOUBLE, MPI_COMM_WORLD);
                }
                
                //compute result
                double m_result = 0;
                int idx;
                
                double * row_ptr = rows_received[row];
                double * column_ptr = columns_received[column];
                   
                for(idx = 0; idx < m2_height; ++idx) m_result += row_ptr[idx] * column_ptr[idx];
                
                //place result in result array
                
                results_arr[i] = m_result;
                
                cell_idx += increment;
            }
            
            // pack and send results
            pos = 0;
            MPI_Pack(results_arr, curr_cells_count, MPI_DOUBLE, result_send_buff, result_send_buff_size, &pos, MPI_COMM_WORLD);
            MPI_Send(result_send_buff, result_send_buff_size, MPI_PACKED, ROOT_ID, 1, MPI_COMM_WORLD);
            
            MPI_Barrier(MPI_COMM_WORLD);

            free(send_buff);
            free(result_send_buff);
        } else {    // -------------- ROOT
            // Create matrixes
            double ** mtrx_1 = allocate_matrix(m1_height, m1_width, 1);
            double ** mtrx_2 = allocate_matrix(m2_height, m2_width, 1);

            fill_matrix(mtrx_1, m1_height, m1_width);
            fill_matrix(mtrx_2, m2_height, m2_width);

            if(verbose_fl){
                print_matrix("First", mtrx_1, m1_height, m1_width);
                print_matrix("Second", mtrx_2, m2_height, m2_width);
            }
            
            MPI_Barrier(MPI_COMM_WORLD);
            //time measure begin <-----------------
            start_time = MPI_Wtime();
            
            {// Send tasks section
                // flags wich detetmines which columns and rows were send already to which process
                char rows_send_per_proc[actual_working_processes][m1_height];
                char cols_send_per_proc[actual_working_processes][m2_width];
               
                int init_i, init_j;
                for(init_i = 0; init_i <= actual_working_processes; ++init_i) 
                {
                    for(init_j = 0 ;init_j < m1_height; ++init_j) rows_send_per_proc[init_i][init_j] = 0;
                    for(init_j = 0 ;init_j < m2_width; ++init_j) cols_send_per_proc[init_i][init_j] = 0;
                }
                
                process_rank = 1;
                long cell_idx;
                
                // send data for computation for each process
                
                for(cell_idx = 0; cell_idx < cells_count; ++cell_idx)
                {
                    int pos = 0;
                    
                    int row =  cell_idx / res_mtrx_width;
                    int column = cell_idx % res_mtrx_width;
                    int send = 0;
                    // prepare
                    if(!rows_send_per_proc[process_rank][row])
                    {
                        MPI_Pack(calc_m_row(mtrx_1, row), 1, m1_row_type, send_buff, send_buff_size, &pos, MPI_COMM_WORLD); 
                        rows_send_per_proc[process_rank][row] = 1;
                        send = 1;
                    }
                    if(!cols_send_per_proc[process_rank][column])
                    {
                        MPI_Pack(calc_m_column(mtrx_2, column), 1, m2_column_type, send_buff, send_buff_size, &pos, MPI_COMM_WORLD);    
                        cols_send_per_proc[process_rank][column] = 1;
                        
                        send = 1;
                    }

                    // send
                    if (send) MPI_Send(send_buff, send_buff_size, MPI_PACKED, process_rank, 0, MPI_COMM_WORLD);
                    
                    // next process rank from round counter
                    process_rank = (process_rank + 1) % (actual_working_processes + 1);
                    if(process_rank == ROOT_ID) process_rank = 1;
                }
            }// Send tasks section end
            
        
            double ** result_mtrx = allocate_matrix(res_mtrx_height, res_mtrx_width, 0);
            double results_arr[cells_per_process[1]];
            
            // receive results from each process and paste into result matrix
            MPI_Status status;
            for(i = 1; i <= actual_working_processes; ++i)
            {
                MPI_Recv(result_send_buff, result_send_buff_size, MPI_PACKED, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
                
                long curr_cells_count = cells_per_process[status.MPI_SOURCE]; // extract cells count from process rank
                int pos = 0;
     
                MPI_Unpack(result_send_buff, result_send_buff_size, &pos, results_arr, curr_cells_count, MPI_DOUBLE, MPI_COMM_WORLD);
                
                int idx;
                int cell_idx = status.MPI_SOURCE - 1;
                int increment = actual_working_processes;
                for(idx = 0; idx < curr_cells_count; ++idx, cell_idx += increment)
                {
                    result_mtrx[cell_idx / res_mtrx_width][cell_idx % res_mtrx_width] = results_arr[idx];
                }
            }
      
            if(verbose_fl) print_matrix("Result", result_mtrx, res_mtrx_height, res_mtrx_width);
            
            MPI_Barrier(MPI_COMM_WORLD);
            //time measure end <-----------------
            end_time = MPI_Wtime();
            
            printf("world_size %10d time_miliseconds %15.5lf\n", world_size, (end_time - start_time) * 1000);
            
            free_uncontinious_matrix(result_mtrx, res_mtrx_height);
            free(result_send_buff);
            free(send_buff);
            free_continious_matrix(mtrx_1);
            free_continious_matrix(mtrx_2);
        }
        
    }
    
    MPI_Finalize();
}
