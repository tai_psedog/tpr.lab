#!/bin/bash

test_process_counts=(2 3  4  5 6 7  8 9 10 11  12 13  14 15 16)
test_shots_count=(2 5 10 20 100)

for process_count in "${test_process_counts[@]}"; do
	for shots_count in "${test_shots_count[@]}"; do
		mpiexec -machinefile ./mpihosts -np $process_count python ./sendcolumnseq.py $shots_count $shots_count $shots_count $shots_count >> result.log
	done
done
