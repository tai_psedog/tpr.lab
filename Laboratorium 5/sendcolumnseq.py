#!/usr/bin/env python
from mpi4py import MPI
import numpy
import sys
def print_result_table(a, width=15):
    print ('%s' * len(a)) % tuple(map(lambda x: str(x).ljust(width), a))


comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size

if size < 2:
    print "World size must be greater than 1"
    comm.abort(comm.MPI_COMM_WORLD, 1)
 
m1_height = int(sys.argv[1])
m1_width = int(sys.argv[2])
m2_height = int(sys.argv[3])
m2_width = int(sys.argv[4])

if m1_width != m2_height:
    print "m1_width != m2_height"
    sys.exit()
 
if rank == 0:
    # two matrices to multiply
    matrixA = numpy.random.uniform(size=(m1_height, m1_width)).astype('d')
    matrixB = numpy.random.uniform(size=(m2_height, m2_width)).astype('d')
    matrixResult = numpy.zeros((m1_height, m2_width), dtype='d')

    #print "matrixA"
    #print matrixA
    #print "matrixB"
    #print matrixB
comm.barrier()
if rank == 0:
    processNumber = 0
    start_time = MPI.Wtime()
    for row in range(m1_height):
        for column in range(m2_width):
            buffer = numpy.concatenate(
                (matrixA[row, :], matrixB[:, column]), axis=0)
            
            comm.Send([buffer, MPI.DOUBLE],
                      dest=processNumber % (size - 1) + 1,
                      tag=rank)
            result = numpy.empty(1, dtype='d')
            comm.Recv([result, MPI.DOUBLE],
                      source=processNumber % (size - 1) + 1,
                      tag=MPI.ANY_TAG)
            matrixResult[row, column] = result[0]
            processNumber += 1
    #print matrixResult
    #comm.barrier()
    finish_time = MPI.Wtime()
    #print_result_table(["#Process workers count", "Time elapsed (MS)", "m1_height", "m1_width", "m2_height","m2_width"])
    print_result_table([size - 1, "%.7f" % ((finish_time - start_time) * 1000), m1_height,m1_width,m2_height,m2_width]) 
else:
    for process in range(0, m1_height*m2_width / (size - 1)):
        buffer = numpy.empty(m2_height + m1_width, dtype='d')
        comm.Recv([buffer, MPI.DOUBLE], source=0, tag=MPI.ANY_TAG)
        result = 0
        for index in range(0, m1_width):
            result += buffer[index] * buffer[m1_width + index]  # rows + columns
        comm.Send([result, MPI.DOUBLE], dest=0, tag=rank)
    if (m1_height*m2_width % (size - 1)) > rank - 1:
        buffer = numpy.empty(m2_height + m1_width, dtype='d')
        comm.Recv([buffer, MPI.DOUBLE], source=0, tag=MPI.ANY_TAG)
        result = 0
        for index in range(0, m1_width):
            result += buffer[index] * buffer[m1_width + index]  # rows + columns
        comm.Send([result, MPI.DOUBLE], dest=0, tag=rank)
