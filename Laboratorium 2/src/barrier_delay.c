#include <stdio.h>
#include <unistd.h>
#include "mpi.h"

#define ITERATION_NUMBER 1000


int main(int argc, char **argv)
{
	int rank, size;
	double total_time = 0.0;
	double start_time = 0.0;
	double end_time = 0.0;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int i = 0;
	for (i=0;i<ITERATION_NUMBER;i++)
	{
	
			start_time = MPI_Wtime();
			MPI_Barrier(MPI_COMM_WORLD);
			end_time = MPI_Wtime();
			
			total_time += end_time - start_time;

	}
	
	if (rank == 0) 
	{
		printf("%.10f\n",total_time/ITERATION_NUMBER);
	}

	MPI_Finalize();
	return 0;
}
