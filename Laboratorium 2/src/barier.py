#!/usr/bin/env python

#------------ CONSTANTS ---------
ITERATIONS_PER_MEASUREMENT = 500
#--------------------------------

from mpi4py import MPI
import numpy

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
process_count = comm.Get_size()


#-------------------- HELP FUNCTIONS -------------------------
def print_result_table(a,width=25):
    print ('%s' * len(a)) % tuple(map(lambda x: str(x).ljust(width), a))

def construct_measurement_function(func, measure_time=False):
    def ft(arg=None):
        start_time = MPI.Wtime()
        for i in range(ITERATIONS_PER_MEASUREMENT):
            if arg is None:
                func()
            else:
                func(arg)
        finish_time = MPI.Wtime()
        return float(finish_time - start_time) / ITERATIONS_PER_MEASUREMENT
    def f(arg=None):
        for i in range(ITERATIONS_PER_MEASUREMENT):
            if arg is None:
                func()
            else:
                func(arg)
    if measure_time:
        return ft
    return f


#----------------------------------------------------------------

barier_time_ms = construct_measurement_function(comm.barrier, measure_time=rank==0)() 

if rank == 0:
    print_result_table(["Process count","Synch time (ms)"], 30)
    print_result_table([comm.Get_size(), barier_time_ms * 1000],30)

MPI.Finalize()
