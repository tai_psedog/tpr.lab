#!/usr/bin/env python

#------------ CONSTANTS ---------
ITERATIONS_PER_MEASUREMENT = 10
ELEMENTS_INCREMENT = 10000
MAX_ELEMENTS_COUNT = 1000000
#--------------------------------

from mpi4py import MPI
import numpy

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
process_count = comm.Get_size()


#-------------------- HELP FUNCTIONS -------------------------
def print_result_table(a, width=25):
    print ('%s' * len(a)) % tuple(map(lambda x: str(x).ljust(width), a))


def construct_measurement_function(func, measure_time=False):
    def ft(arg=None):
        start_time = MPI.Wtime()
        for i in range(ITERATIONS_PER_MEASUREMENT):
            if arg is None:
                func()
            else:
                func(arg)
        finish_time = MPI.Wtime()
        return float(finish_time - start_time) / ITERATIONS_PER_MEASUREMENT
    def f(arg=None):
        for i in range(ITERATIONS_PER_MEASUREMENT):
            if arg is None:
                func()
            else:
                func(arg)
    if measure_time:
        return ft
    return f

#------------------- P2P BROADCAST FUNCTIONS ---------------------


def do_p2p_broadcast_send(data):
    for target in range(1, process_count):
        comm.send(data, dest=target)


def do_p2p_broadcast_receive(data=None):
    comm.recv(source=0)

measure_p2p_broadcast = construct_measurement_function(do_p2p_broadcast_send, measure_time=True)
receive_p2p_bc = construct_measurement_function(do_p2p_broadcast_receive)

#------------------- MPI BROADCAST FUNCTIONS ---------------------


def do_broadcast(data=None):
    data = comm.bcast(data, 0)

measure_mpi_broadcast =  construct_measurement_function(do_broadcast,measure_time=True)
receive_mpi_bc = construct_measurement_function(do_broadcast)

#------------------- SCATTER FUNCTIONS --------------------------


def do_mpi_scatter(data=None):
    recv_count = [data.size / process_count for _ in range(process_count)]
    recv_count[process_count-1] += data.size - sum(recv_count)
    recv_buff = numpy.empty(recv_count[rank], dtype='i')
    comm.Reduce_scatter(data, recv_buff,recv_count)

measure_mpi_scatter = construct_measurement_function(do_mpi_scatter, measure_time=True)
receive_mpi_sc = construct_measurement_function(do_mpi_scatter)

#----------------------------------------------------------------
if rank == 0:
    print_result_table(["Size (B)", "P2P BROADCAST t. (S)", "MPI BROADCAST t. (S)", "SCATTER RED t. (S)"])

for curr_element_count in range(ELEMENTS_INCREMENT, MAX_ELEMENTS_COUNT + ELEMENTS_INCREMENT, ELEMENTS_INCREMENT):
    data = numpy.arange(curr_element_count, dtype='i')

    if rank == 0:
        p2p_bc_t = measure_p2p_broadcast(data)
        mpi_bc_t = measure_mpi_broadcast(data)
        p2p_sc_t = measure_mpi_scatter(data)

        print_result_table([data.nbytes, p2p_bc_t, mpi_bc_t, p2p_sc_t])
    else:
        receive_p2p_bc()
        receive_mpi_bc()
        receive_mpi_sc(data)

MPI.Finalize()
