#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>

#define ITERATION_NUMBER 50
#define MAX_ELEMENTS 1000000

void my_bcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator) 
{
	int world_rank;
	int world_size;
  
	MPI_Comm_rank(communicator, &world_rank);
	MPI_Comm_size(communicator, &world_size);

	if (world_rank == root) 
	{
		int i;
		for (i = 0; i < world_size; i++) 
		{
		  if (i != world_rank) 
		  {
			MPI_Send(data, count, datatype, i, 0, communicator);
		  }
		}
	} 
	else 
	{
		MPI_Recv(data, count, datatype, root, 0, communicator, MPI_STATUS_IGNORE);
	}
}

int main(int argc, char** argv) 
{


	
	int num_elements = 0;
	
	MPI_Init(NULL, NULL);
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	
	if (world_rank == 0) 
	{
		printf("ELEMENTS\tDATA SIZE\tMY_BCAST\tMPI_BCAST\n");
	}
	
	for (num_elements =0; num_elements<MAX_ELEMENTS; num_elements+=10)
	{
		double total_my_bcast_time = 0.0;
		double total_mpi_bcast_time = 0.0;
		int i;
		int* data = (int*)malloc(sizeof(int) * num_elements);
		assert(data != NULL);
	 
		if(data!=NULL)
		{
			for (i = 0; i < ITERATION_NUMBER; i++) 
			{
				MPI_Barrier(MPI_COMM_WORLD);
				total_my_bcast_time -= MPI_Wtime();
				my_bcast(data, num_elements, MPI_INT, 0, MPI_COMM_WORLD);
				MPI_Barrier(MPI_COMM_WORLD);
				total_my_bcast_time += MPI_Wtime();

				MPI_Barrier(MPI_COMM_WORLD);
				total_mpi_bcast_time -= MPI_Wtime();
				MPI_Bcast(data, num_elements, MPI_INT, 0, MPI_COMM_WORLD);
				MPI_Barrier(MPI_COMM_WORLD);
				total_mpi_bcast_time += MPI_Wtime();
			}

			if (world_rank == 0) 
			{
				printf("%d\t%d\t%.10f\t%.10f\n",num_elements,num_elements*(int)sizeof(int),total_my_bcast_time/ITERATION_NUMBER, total_mpi_bcast_time/ITERATION_NUMBER);
			}
		}
		else
		{
			printf("Unable to allocate memory");
		}

	}
	MPI_Finalize();
}
