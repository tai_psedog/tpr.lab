#!/usr/bin/env python

from random import random
from math import hypot
import argparse
import time

#--------------- HELP FUNCTIONS ------------------

def measure_time(function_to_measure):
    def decorator_function(*args):
        start = time.time()
        function_result = function_to_measure(*args)
        end = time.time()
        return end - start, function_result
    return decorator_function

def count_inner_shots(shots_count):
    count_inside = 0
    for count in xrange(0, shots_count):
        d = hypot(random(), random())
        if d < 1: count_inside += 1
    return count_inside

def estimate_pi(count_inside, count):
    return 4.0 * count_inside / count

@measure_time
def compute_pi(shots_count):
    count_inside = count_inner_shots(shots_count)
    return estimate_pi(count_inside, shots_count)

#-------------------------------------------------

argparser = argparse.ArgumentParser()
argparser.add_argument('shots', metavar='N', type=int,help='count of shots for pi estimation')
args = argparser.parse_args()

results = compute_pi(args.shots)
print "computed pi %f in %f miliseconds" % (results[1], results[0] * 1000)