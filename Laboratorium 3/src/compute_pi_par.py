#!/usr/bin/env python

from random import random, seed
from math import hypot
from mpi4py import MPI
import sys
import numpy
from time import time

#--------------- HELP FUNCTIONS ------------------

def print_result_table(a, width=25):
    print ('%s' * len(a)) % tuple(map(lambda x: str(x).ljust(width), a))

def count_inner_shots(shots_count):
    count_inside = 0
    for count in xrange(0, shots_count):
        d = hypot(random(), random())
        if d < 1: count_inside += 1
    return count_inside

def estimate_pi(count_inside, count):
    return 4.0 * count_inside / count

def compute_pi(shots_count, comm, process_count, rank):
    local_process_shots_count = shots_count / process_count
    if rank == 0:
    	local_process_shots_count = shots_count - local_process_shots_count * (process_count - 1)
    local_count_inside = count_inner_shots(local_process_shots_count)
    
    global_count_inside = numpy.array(0, dtype='i')
    local_count_inside = numpy.array(local_count_inside, dtype='i')
    comm.Reduce(local_count_inside, 
    			global_count_inside, 
    			op=MPI.SUM, root=0)

    if rank == 0:
    	return estimate_pi(global_count_inside, shots_count)

#-------------------------------------------------

if len(sys.argv) != 2:
	print "usage: %s shots_count" % sys.argv[0]
	sys.exit()

#------------------ MPI Vals --------------------
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
process_count = comm.Get_size()
shots_count = int(sys.argv[1])

seed(time() + rank * process_count)

comm.barrier()
if rank == 0:
	start_time = MPI.Wtime()

result = compute_pi(shots_count, comm, process_count, rank)
comm.barrier()

if rank == 0:
	finish_time = MPI.Wtime()
	print_result_table(["#Process count", "Shots count", "Time elapsed (MS)", "Result"])
	print_result_table([process_count, shots_count, (finish_time - start_time) * 1000 , result])