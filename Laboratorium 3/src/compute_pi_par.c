#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#define MASTER 0
#define ITERATION_NUMBER 1000

int main(int argc, char **argv) {

	if(argc != 2){
		return -1;
	}


	int rank, size;
    int i, it, j;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	srand(time(NULL)+rank);
	int points = atoi(argv[1]);

	double start_time;
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == MASTER)
	{
		start_time = MPI_Wtime();
	}

	MPI_Bcast(&points, 1, MPI_INT, MASTER, MPI_COMM_WORLD);

	int points_to_rand = points / size;
	int points_in_target = 0;
	for (i = 0; i < points_to_rand; ++i) {
		double x = ((double) rand() / (RAND_MAX)) * 2 - 1;
		double y = ((double) rand() / (RAND_MAX)) * 2 - 1;

		if (x*x + y*y <= 1) {
			++points_in_target;
		}
	}

	int result;
	MPI_Reduce(&points_in_target, &result, 1, MPI_INT, MPI_SUM, MASTER, MPI_COMM_WORLD);

	if (rank == MASTER) {
		double end_time = MPI_Wtime();
		double duration_time = end_time - start_time;
		double pi = 4.0 * result / points;

		//printf("Process count\tPoints\tTime\n");
		printf("%d\t%d\t%1.6f\n",size,points,duration_time);
	}
	MPI_Barrier(MPI_COMM_WORLD);	
    MPI_Finalize();
    return 0;
}
