#!/bin/bash

test_process_counts=(2 3  4  5 6 7  8 9 10 11  12 13  14 15 16 17  18 19 20  21 22  23 24)
test_shots_count=(1000 5000 10000 15000 30000 40000 50000 75000 100000 150000 200000 250000 400000 500000 600000 750000 900000 1000000 1250000 1500000 1750000 2000000 2250000 2500000 2750000 3000000)

for process_count in "${test_process_counts[@]}"; do
	for shots_count in "${test_shots_count[@]}"; do
		mpiexec -np $process_count ./compute_pi_par.py $shots_count >> result.log
	done
done
