#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROOT_ID 0

double ** create_matrix(int height, int width)
{
  double ** result = (double**) malloc(height * sizeof(double*));
  double * buff = (double*) malloc(height * width * sizeof(double));
  int idx = 0;
  for (idx = 0; idx < height; ++idx) {
    *(result + idx) = buff + idx * width;
  }
  
  return result;
}

void fill_matrix(double ** matrix, int height, int width)
{
  int i, j, c;
  c = 1;
  for(i = 0; i < height; ++i)
  {
    for(j = 0; j < width; ++j)
      matrix[i][j] = c++;
  }
}

void print_vector(double * vector, int size)
{
  int j;
  for (j = 0; j < size; ++j)
    printf("%6.1lf", vector[j]);
  putchar('\n');
}

void print_matrix(double **matrix, int h, int w)
{
  int i;
  for (i = 0; i < h; ++i) {
    print_vector(matrix[i], w);
  }
  putchar('\n');
}

double * calculate_next_row(double ** table, int number)
{
  return table[number];
}

double * calculate_next_column(double ** table, int number)
{
  return *table + number;
}

int main(int argc, char **argv)
{
  if (argc < 4) {
    printf("usage: %s matrix_height matrix_width send_by_row_flag\n", *argv);
    return;
  }

  int height = atoi(argv[1]);
  int width = atoi(argv[2]);
  int send_by_row = atoi(argv[3]);
  
  MPI_Init(NULL, NULL);
  
  int world_rank;
  int world_size;
  
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  if (world_size < 2) {
    fprintf(stderr, "World size must be greater than 1 for %s\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  MPI_Status status;
  MPI_Datatype type;
  
  int receive_buf_lenght;
  int sending_chunks_count;
  double * (* chunk_calculation_f)(double **, int);
  
  if(send_by_row)
  {
    MPI_Type_vector(1, width, 0, MPI_DOUBLE, &type); 
    receive_buf_lenght = width;
    sending_chunks_count = height;
    chunk_calculation_f = calculate_next_row;
  }else{
    MPI_Type_vector(height, 1, width, MPI_DOUBLE, &type);
    receive_buf_lenght = height;
    sending_chunks_count = width;
    chunk_calculation_f = calculate_next_column;
  }
  
  MPI_Type_commit(&type);

  int i, j;
  if (world_rank != ROOT_ID) {
    double buf[receive_buf_lenght];
  
    MPI_Recv(buf, receive_buf_lenght, MPI_DOUBLE, ROOT_ID, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
    printf("[%2d]RECV: ", world_rank);
    print_vector(buf, receive_buf_lenght);
  
  } else {
    double ** tab = create_matrix(height, width);
    fill_matrix(tab, height, width);
    print_matrix(tab, height, width);
    
    for (i = 1; i < sending_chunks_count; i++) {
      if (i >= world_size) {
          break;
      }
      MPI_Send(chunk_calculation_f(tab, i - 1), 1, type, i, 0, MPI_COMM_WORLD);
    }
    
    free(*tab);
    free(tab);
  }
  MPI_Finalize();
}
